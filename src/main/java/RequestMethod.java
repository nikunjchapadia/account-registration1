package main.java;

/**
 * <code>RequestMethod</code>
 *
 * @author Nikunj Chapadia
 * @version 7/11/15 1:29 PM (11 Jul 2015)
 */
public enum RequestMethod {
    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;
}
